# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=virtctl
pkgver=1.2.2
pkgrel=0
pkgdesc="CLI client for KubeVirt - the Kubernetes Virtualization API"
url="https://kubevirt.io/"
arch="all"
license="Apache-2.0"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/kubevirt/kubevirt/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/kubevirt-$pkgver"

_goldflags="
-X kubevirt.io/client-go/version.gitVersion=v$pkgver
-X kubevirt.io/client-go/version.gitCommit=AlpineLinux
-X kubevirt.io/client-go/version.gitTreeState=clean
-X kubevirt.io/client-go/version.buildDate=0
"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -ldflags "$_goldflags" ./cmd/virtctl/virtctl.go

	for shell in bash fish zsh; do
		./$pkgname completion $shell > $pkgname.$shell
	done
}

check() {
	# Only run unittest for virtctl
	go test -ldflags "$_goldflags" ./pkg/virtctl/...
}

package() {
	install -Dm755 $pkgname -t "$pkgdir"/usr/bin/

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish
	install -Dm644 $pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
d40e25fc9ce7e6a7ba991f86b2de03b92bcdf3644dfb2e2b3bcd739f88e7cc9accbbd5c853b74f836852feae3a994b9152c2dae4ee86729fe67330fcc8755542  virtctl-1.2.2.tar.gz
"
