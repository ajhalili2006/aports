# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-nm
pkgver=6.1.2
pkgrel=0
pkgdesc="Plasma applet written in QML for managing network connections"
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="(LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.0-or-later"
depends="
	kirigami
	networkmanager
	"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kcompletion-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kservice-dev
	ksvg-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	mobile-broadband-provider-info
	modemmanager-qt-dev
	networkmanager-qt-dev
	libplasma-dev
	prison-dev
	qca-qt6-dev
	qcoro-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	samurai
	solid-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/plasma/plasma-nm.git"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-nm-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib	
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

mobile() {
	pkgdesc="$pkgdesc (mobile KCM's)"
	depends="$depends $pkgname"

	amove usr/share/kpackage
	amove usr/lib/qt6/plugins/kcms
}

sha512sums="
f1e4f5a143f107dc5ba10f4b82a4ba021cc0a1db9ea05f90172228e38a0fbaa0d51de1f18b7d4a714ad145fefac46b3a3a08a24367f5a1f2c9d53e32cdae1cf4  plasma-nm-6.1.2.tar.xz
"
