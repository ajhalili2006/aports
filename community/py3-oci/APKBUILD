# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-oci
pkgver=2.129.1
pkgrel=0
pkgdesc="Oracle Cloud Infrastructure Python SDK"
url="https://docs.oracle.com/en-us/iaas/tools/python/2.53.1/index.html"
arch="noarch"
license="Apache-2.0"
depends="py3-certifi py3-circuitbreaker py3-cryptography py3-dateutil py3-openssl py3-tz"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest py3-vcrpy"
subpackages="$pkgname-pyc"
source="$pkgname.$pkgver.tar.gz::https://github.com/oracle/oci-python-sdk/archive/refs/tags/v$pkgver.tar.gz
	vcr.patch
	"
builddir="$srcdir/oci-python-sdk-$pkgver"
options="!check" # argparse conflict (?)

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest tests
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
d5e8e857f5bae49986403e4f87ccb727ec7f2c7ad0c0bbfc1a5fd7829fcae356779602fda331588a40ad12933b20ac439b488a5eab6d4c0874c8d86dea8855a5  py3-oci.2.129.1.tar.gz
e88495f19a3b9bd4b4b086007e2c93d6200aa316e93c1ec58b31794afb58967994f061a5ad1346edbbecd9119cea7a60c1e2ac6cba99f78b4e349b8f594ce01f  vcr.patch
"
