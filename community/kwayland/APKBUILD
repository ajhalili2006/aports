# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kwayland
pkgver=6.1.2
pkgrel=0
pkgdesc="Qt-style Client and Server library wrapper for the Wayland libraries"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://www.kde.org"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	qt6-qtwayland-dev
	wayland-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	plasma-wayland-protocols
	qt6-qtbase-dev
	qt6-qttools-dev
	wayland-protocols
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-dev $pkgname-doc $pkgname-dbg"
_repo_url="https://invent.kde.org/plasma/kwayland.git"
source="https://download.kde.org/stable/plasma/$pkgver/kwayland-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c9ce5e450329ce08c3e26675f9999ff738e06a6b78b8fbaa489f1ef29cb8b95b9590589703b1872d52afd1ffad0fb23bbe68d8c10489e133c2377e2f51d66fd1  kwayland-6.1.2.tar.xz
"
