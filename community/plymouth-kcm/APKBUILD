# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plymouth-kcm
pkgver=6.1.2
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/plasma/plymouth-kcm"
pkgdesc="KCM to manage the Plymouth (Boot) theme"
license="GPL-2.0-or-later"
depends="plymouth"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	plymouth-dev
	qt6-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/plasma/plymouth-kcm.git"
source="https://download.kde.org/stable/plasma/$pkgver/plymouth-kcm-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
51d76d724b7cd835eaae2b24861ab58f393c7478da556412642be02ec50c15ff235874335999e5b79f766a0a58843404c971ba8d53a82f8a05d117acfb513890  plymouth-kcm-6.1.2.tar.xz
"
